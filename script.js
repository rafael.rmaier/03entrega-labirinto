/*PLANO
1. MAPEAR O LABIRINTO
2. Atribuir IDs com 'x' e 'y' a cada celula
3. Atravessavel ou nao (dataset? boolean?)
4. Estilizar no CSS
5. Talvez criar um novo contexto pra div jogador
6. Div jogador com atributos de posição
7. Listeners para cada tecla. Adicionar à posição relevante dependendo do arrow.
8. Append na div correspondente. */

//Labirinto
const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

//Variaveis Globais
const main = document.getElementsByTagName("main")[0];
const maze = document.getElementById("maze");
const container = document.getElementById('container');
const divDefeat = document.getElementById("defeat");
const divVictory = document.getElementById("victory");
let arrCells = [];
const buttons = document.getElementsByClassName("retry");
const end = document.getElementById("finish");
//Player

const imgPlayer = document.getElementById("player");


//Montando o labirinto
for (let index = 0; index < map.length; index++) {

    const element = map[index];
    const row = document.createElement("tr");
    row.classList.add("row");
    maze.appendChild(row);
    for (let j = 0; j < element.length; j++) {
        let cell = document.createElement("td");
        cell.dataset.eixoX = index;
        cell.dataset.eixoY = j;

        switch (element[j]) {
            case " ":
                cell.classList.add("cell__empty");

                break;
            case "W":
                cell.classList.add("cell__wall");

                break;
            case "S":
                cell.classList.add("cell__empty");
                cell.id = "cell--start";
                cell.appendChild(imgPlayer);
                imgPlayer.dataset.eixoX = index;
                imgPlayer.dataset.eixoY = j;
                break;
            case "F":
                cell.classList.add("cell__empty");
                cell.classList.add("cell__end");

                cell.appendChild(end);
                break;
            default:
                break;

        }
        arrCells.push(cell);
        row.appendChild(cell);

    }

}


//Listener de teclado
document.addEventListener('keydown', (event) => {
    const keyName = event.key;




    const newPos = movePlayer(keyName);
    setTimeout(checkVictory, 500, newPos);



});

//Listener dos botões
for (let index = 0; index < buttons.length; index++) {
    const element = buttons[index];
    element.addEventListener('click', function() {
        window.location.reload();
    });

}

//Função de movimento
function movePlayer(keyName) {
    let xToInt = parseInt(imgPlayer.dataset.eixoX, 10);
    let yToInt = parseInt(imgPlayer.dataset.eixoY, 10);
    const posX = imgPlayer.dataset.eixoX;
    const posY = imgPlayer.dataset.eixoY;
    switch (keyName) {
        case "ArrowUp":
            xToInt -= 1;
            break;
        case "ArrowDown":
            xToInt += 1;
            break;
        case "ArrowLeft":
            yToInt -= 1;
            break;
        case "ArrowRight":
            yToInt += 1;
            break;
        default:
            break;
    }
    for (let index = 0; index < arrCells.length; index++) {
        let el = arrCells[index];
        if ((xToInt == el.dataset.eixoX) && (yToInt == el.dataset.eixoY)) {
            if (el.classList.contains("cell__empty") && !(el.classList.contains("cell__end"))) {


                el.appendChild(imgPlayer);
                imgPlayer.dataset.eixoX = xToInt;
                imgPlayer.dataset.eixoY = yToInt;
                return el;
            } else if (el.classList.contains("cell__end")) {
                imgPlayer.style.position = "absolute";
                end.style.display = "none";
                el.appendChild(imgPlayer);
                imgPlayer.dataset.eixoX = xToInt;
                imgPlayer.dataset.eixoY = yToInt;
                return el;

            }
        }
    }
    return imgPlayer;
}




//Checar derrota e vitória
function checkDefeat(element) {
    if (element.classList.contains("cell__wall")) {
        container.style.display = "none";
        divDefeat.style.display = "flex";
    }

}

function checkVictory(element) {
    if (element.classList.contains("cell__end")) {
        container.style.display = "none";
        divVictory.style.display = "flex";


    }

}